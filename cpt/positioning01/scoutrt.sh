#!/bin/bash
#
#MG17: ScoutRT startup script through XRDP
#

# Start ScoutRT with a personalized openbox desktop
cd /home/proton/ScoutRT/
openbox --config-file /etc/scoutrt/scoutrt_openbox_rc.xml & sh /home/proton/ScoutRT/ScoutRT.sh

# Unmount mapped drives just in case
/usr/bin/fusermount -u ~/thinclient_drives

#Kill all user processes when we disconnect
killall -u $USER

#Kill all user processes without giving a chance
killall -u $USER -s SIGKILL
