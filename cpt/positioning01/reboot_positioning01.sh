#!/bin/bash

#Script to cleanly kill all ScoutRT instances and reboot
#MG17 2024.05.24

#killall ScoutRT processes (there should be only one)
/bin/killall ScoutRT


#Wait 10 seconds
/bin/sleep 10

#Reboot the server
/sbin/reboot
