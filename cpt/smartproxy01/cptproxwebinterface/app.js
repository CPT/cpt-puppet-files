const express = require('express');
const path = require('path');
const { exec } = require('child_process');
const passport = require('passport');
const flash = require('express-flash');
const session = require('express-session');
const winston = require('winston')
const https = require('https')
const fs = require('fs')
const nlbr = require('nl2br')

const httpsport = 443;
const app = express();

const initializePassport = require('./passport-config');
const { urlencoded} = require('express');

/*configure and add the needed keys and certificates for an ssh connection*/
const httpsoptions = {
    cert: fs.readFileSync('/etc/pki/tls/private/cptvirt-linux50.psi.ch.crt.pem'),
    key: fs.readFileSync('/etc/pki/tls/private/smartproxy01.key')
};

/*configuration of the logging by winston
- Don't forget to give write rights to app.log on Linux
*/
const logger = winston.createLogger({
    format: winston.format.combine(
        // { format: 'YYYY-MM-DD HH:mm:ss.SSSZZ' } is for the implementation of the timezone so the timestamp is applied correctly
        winston.format.timestamp(
            { format: 'YYYY-MM-DD HH:mm:ssZZ' }
        ),
        winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
        
    ),
    transports: [
        /*configure the destination for the winston log file
        level: info is important it will not work*/
        new winston.transports.File({
            filename: 'app.log',
            level: 'info',
            handleExceptions: true,
        }),

        new winston.transports.File({
            filename: 'error.log',
            level: 'error',
            handleExceptions: true
        }),

        new winston.transports.Console({
            level: 'info'
        })
    ]
});

var authorizedUsers = fs.readFileSync('users.conf').toString().split('\n')
//console.log(authorizedUsers)


var squidEndTimeClean
var squidStartTime = 0
var squidEndTime = 0
var logfiletext = ''
var squidlogfiletext = ''
var timeouthandle = ''


initializePassport(passport, authorizedUsers)

app.set('view-engine','ejs');
app.use(express.static(__dirname + '/public'));
app.use(urlencoded({extended: false}));
app.use(flash())
app.use(session({
    secret: 'anything',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 600000
    }
}))

app.use(passport.initialize());
app.use(passport.session());

//app.use('/public',express.static(path.join(__dirname, 'static')));

/**Define what happens when the root directory of the web interface is called*/
app.get('/', checkAuthenticated, checkServer, readLogFiles, (req,res)=>{
    if(squidEndTime < Date.now()) squidEndTimeClean = null;
    console.log(serverstate)
    res.render('index.ejs', {
        serverstate:serverstate,
        squidEndTimeClean:squidEndTimeClean,
        logfile:logfiletext,
        squidlogfile:squidlogfiletext,
        squidStartTime: squidStartTime,
        squidEndTime:squidEndTime
    });
});

app.post('/startProxy', checkAuthenticated, (req,res)=>{
    //console.log('Button A works')
    startProxy(req,res)
});

app.post('/stopProxy', checkAuthenticated, (req,res)=>{
    //console.log('Button B works')
    squidEndTimeClean = ''
    timeouthandle = ''
    squidEndTime = ''
    stopProxy(req,res)
    return res.redirect('/')
});

/*Function to time a proxy uptime*/
app.post('/timeProxy', checkAuthenticated, (req,res)=>{
    squidStartTime = Date.now()
    
    //console.log(`It is now ${getDate(timestamp)}`)
    let runningtime = req.body.proxyTime * 60000
    //console.log(timeouthandle)
    if(timeouthandle == ''){
        startProxy(req,res)
        timeouthandle = setTimeout(()=>{
        stopProxy(req,res)
        //console.log('Something worked' + Date.now())
    }, runningtime)
    }else{
        clearTimeout(timeouthandle)
        timeouthandle = setTimeout(()=>{
            stopProxy(req,res)
            //console.log('Something worked' + Date.now())
        }, runningtime)
		squidEndTime = squidStartTime + runningtime
        squidEndTimeClean = `The Proxy is enabled until ${getDate(squidStartTime + runningtime)}`
        logger.info(`${req.user} attempted to reset the proxy for ${req.body.proxyTime} minutes until ${getDate(squidEndTime)}` )																																		  
        return res.redirect('/')
    }
    squidEndTime = squidStartTime + runningtime
    squidEndTimeClean = `The Proxy is enabled until ${getDate(squidStartTime + runningtime)}`
    logger.info(`${req.user} attempted to start the proxy for ${req.body.proxyTime} minutes until ${getDate(squidEndTime)}` ) 
    
})

/**Define what happens when the /login directory of the web interface is called*/
app.get('/login', checkNotAuthenticated, (req, res)=>{
    
    res.render('login.ejs');
    //console.log(getDate(Date.now()))
    //console.log('Got here on login')
})

app.post('/login', checkNotAuthenticated ,passport.authenticate('local',{
    failureRedirect: '/login',
    failureFlash: true
}
), function (req, res){
    //console.log(type(req.user))
    //console.log(req.user)
    logger.info(`${req.user} has logged in`);
    return res.redirect('/')
})

function checkAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}

function checkNotAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return res.redirect('/')
    }
    next()
}

function checkServer(req,res,next){
    //console.log('server checked');
    exec('sudo systemctl is-active squid',(err, stdout, stderr)=>{
        if(err){
            console.log(`error: ${err.message}`);
            //setTimeout(()=>res.sendFile(path.join(__dirname,'static','index.html')),10000)
        } 
        if(stderr){
             console.log(`stderr: ${stderr.message}`);
             //res.send(`stderr: ${stderr}`);
        }
        else {
            //console.log('Server checked');
            serverstate = stdout.toString()
            console.log(serverstate)
            if (serverstate != 'active\n')
                serverstate = 'inactive'
            
        }
        return next()
    })
}

function startProxy(req, res){
    exec('sudo systemctl start squid',(err,stdout,stderr)=>{
        if(err){
            console.log(`error: ${err.message}`);
            logger.error(`error: ${err.message}`)
            //res.send(`error: ${err.message}`);
            //setTimeout(()=>res.sendFile(path.join(__dirname,'static','index.html')),10000)
            return
        } 
        if(stderr){
             console.log(`stderr: ${stderr}`);
             logger.error(`stderr: ${stderr}`)
             //res.send(`stderr: ${stderr}`);
        }
        else console.log('Squid has started with no Errors');
        logger.info(`${req.user} has started Squid with no Errors`) 
        //res.send(stdout);
        return res.redirect('/')
     });
}

function stopProxy(req,res){
    exec('sudo systemctl stop squid',(err,stdout,stderr)=>{
        if(err){
            console.log(`error: ${err.message}`);
            logger.error(`error: ${err.message}`)
            //setTimeout(()=>res.sendFile(path.join(__dirname,'static','index.html')),10000)
            return
        } 
        if(stderr){
             console.log(`stderr: ${stderr.message}`);
             logger.error(`stderr: ${stderr}`)
             //res.send(`stderr: ${stderr}`);
             
        }
        else {
        //console.log('Squid has stopped with no Errors');
        logger.info(`${req.user} has stopped Squid with no Errors`)
        squidEndTimeClean = ''
        timeouthandle = ''
        squidEndTime = ''
        
        
        
    
    }
    });
}

function getDate(timestamp){
    timestampobj = new Date(timestamp)
    //return timestampobj.getHours + ':' + timestampobj.getMinutes+ ':'+ timestampobj.getSeconds+ ' on the '+ timestampobj.getDate+'.'+timestampobj.getMonth+'.'+timestampobj.getFullYear
    return `${(`0${timestampobj.getHours()}`).slice(-2)}:${(`0${timestampobj.getMinutes()}`).slice(-2)}:${(`0${timestampobj.getSeconds()}`).slice(-2)} on the ${(`0${timestampobj.getDate()}`).slice(-2)}.${(`0${timestampobj.getMonth()+1}`).slice(-2)}.${timestampobj.getFullYear()}`
    
}

function readLogFiles(req, res, next){
    try{
        logfiletext = nlbr(fs.readFileSync('app.log'))
    }
    catch(e){
        logfiletext= 'Logfile could not be found'
        logger.error(e)
    }
    try{
        //console.log(Date.now())
        squidlogfiletext = nlbr(fs.readFileSync('/var/log/squid/access.log').toString()).split('\n')
        //console.log(squidlogfiletext)
    }
    catch(e){
        squidlogfiletext ='Logfile could not be found'
        logger.error(e)
    }

    return next()
    
}

https.createServer(httpsoptions, app).listen(httpsport, ()=> {
});

