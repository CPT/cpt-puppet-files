const LocalStrategy = require('passport-local').Strategy
var pam = require('authenticate-pam')


function initialize(passport, authorizedUsers){
     const authenticateUser = function(username, password ,done){
         //console.log(username, password)
         if(authorizedUsers.includes(username)){
           pam.authenticate(username ,password, (err)=>{
            if(err) {
                done(null, false, {message: 'The Username or Password is wrong, please try again'});
              }else{
                done(null, username);
              }
         })

         }else{
             done(null,false, {message: 'User not authorized'})  
         }
         
     /*   const user = getUserByName(name)
        if (user == null){
            console.log('Username not found')
            return done(null, false, {message: 'No user with that name'})
        }
        if(password == user.password){
            console.log('Password found')
            return done(null, user)
            
        }else{
            console.log('Password not found')
            return done(null, false, {message: 'Password incorrect'})
            } */
        }  
    passport.use(new LocalStrategy({
        usernameField: 'name',
        passwordField: 'password',
    }, authenticateUser))
    passport.serializeUser((username,done)=>done(null,username))
    passport.deserializeUser((username,done)=>{
        return done(null, username)
    })
}

module.exports = initialize