---
services:
# DCM4CHEE uses the an LDAP server for configuration, as per the DICOM 
# Application Configuration Management Profile. For this installation
# we do not apply any custom configuration to DCM4CEE
  ldap:
    container_name: ldap
    image: dcm4che/slapd-dcm4chee:2.6.0-25.1
    env_file: docker-compose.env
    networks:
      - pacs_network

# DCM4CHEE uses this PostgreSQL database
  pacs-postgres:
    container_name: pacspostgres
    image: dcm4che/postgres-dcm4chee:13.5-25
    logging:
      driver: json-file
      options:
        max-size: "10m"
    environment:
      POSTGRES_DB: kheops_pacs
      POSTGRES_USER: kheops_pacs
      POSTGRES_PASSWORD_FILE: /run/secrets/kheops_pacsdb_pass
    env_file: docker-compose.env
    volumes:
      - dcm4chee-db-data:/var/lib/postgresql/data
    secrets:
      - kheops_pacsdb_pass
    networks:
      - pacs_network

# The main DCM4CHEE container
  pacs-arc:
    container_name: pacsarc
    image: dcm4che/dcm4chee-arc-psql:5.25.1
    env_file: docker-compose.env
    environment:
      POSTGRES_DB: kheops_pacs
      POSTGRES_USER: kheops_pacs
      POSTGRES_PASSWORD_FILE: /run/secrets/kheops_pacsdb_pass
      STORAGE_DIR: /storage/fs1
      POSTGRES_HOST: pacspostgres
      WILDFLY_CHOWN: /storage /opt/wildfly/standalone
      WILDFLY_WAIT_FOR: ldap:389 pacspostgres:5432
      JAVA_OPTS: -Xms64m -Xmx512m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true
    secrets:
      - kheops_pacsdb_pass
    depends_on:
      - ldap
      - pacs-postgres
    volumes:
      - dcm4chee-arc-wildfly:/opt/wildfly/standalone
      - dcm4chee-arc-storage:/storage
    networks:
      - pacs_network

# KHEOPS persists all data to this PostgreSQL database
  kheops-postgres:
    container_name: kheopspostgres
    image: postgres:12.9-alpine
    volumes:
      - kheops-db-data:/var/lib/postgresql/data
    environment:
      POSTGRES_DB: kheops
      POSTGRES_USER: kheopsuser
      POSTGRES_PASSWORD_FILE: /run/secrets/kheops_authdb_pass
    secrets:
      - kheops_authdb_pass
    networks:
      - kheops_network

  kheops-authorization:
    container_name: kheopsauthorization
    image: osirixfoundation/kheops-authorization:v1.0.8
    logging:
      driver: json-file
      options:
        max-file: "10"
        max-size: "10m"
    volumes:
      - logs_auth:/usr/local/tomcat/logs
    env_file: docker-compose.env
    environment:
      CATALINA_OPTS: -Duser.timezone=CET
    depends_on:
      - kheops-postgres
    secrets:
      - kheops_auth_hmasecret
      - kheops_authdb_pass
      - kheops_client_dicomwebproxysecret
      - kheops_client_zippersecret
    networks:
      - kheops_network
      - beats_network
    extra_hosts:
      - "keycloak01.psi.ch:172.30.73.42"

  kheops-authorization-metricbeat:
    container_name: kheopsauthorizationmetricbeat
    image: osirixfoundation/kheops-authorization-metricbeat:v1.0.8
    env_file: docker-compose.env
    environment:
      - KHEOPS_INSTANCES=kheops
      - KHEOPS_LOGSTASH_URL=logstash:5044
    depends_on:
      - kheops-authorization
    networks:
      - beats_network

  kheops-filebeat-sidecar:
    container_name: kheopsfilebeatsidecar
    image: osirixfoundation/kheops-filebeat-sidecar:v1.0.8
    environment:
      - KHEOPS_INSTANCES=kheops
      - KHEOPS_LOGSTASH_URL=logstash:5044
    env_file: docker-compose.env
    depends_on:
      - kheops-authorization
      - kheops-reverse-proxy
      - pacs-authorization-proxy
    volumes:
      - logs_auth:/kheops/authorization/logs
      - logs_pep:/kheops/pep/logs
      - logs_reverse_proxy:/kheops/reverseproxy/logs
      - filebeat_registry:/registry
    networks:
      - beats_network

  kheops-zipper:
    container_name: kheopszipper
    image: osirixfoundation/kheops-zipper:v1.0.8
    logging:
      driver: json-file
      options:
        max-file: "10"
        max-size: "10m"
    env_file: docker-compose.env
    secrets:
      - kheops_client_zippersecret
    networks:
      - kheops_network

  kheops-ui:
    container_name: kheopsui
    image: osirixfoundation/kheops-ui:v1.0.8
    env_file: docker-compose.env
    networks:
      - frontend_network

  kheops-dicomweb-proxy:
    container_name: kheopsdicomwebproxy
    image: osirixfoundation/kheops-dicomweb-proxy:v1.0.8
    logging:
      driver: json-file
      options:
        max-file: "10"
        max-size: "10m"
    env_file: docker-compose.env
    secrets:
      - kheops_auth_hmasecret_post
      - kheops_client_dicomwebproxysecret
    networks:
      - kheops_network

  kheops-reverse-proxy:
    image: osirixfoundation/kheops-reverse-proxy:v1.0.8-secure
    container_name: kheopsreverseproxy
    env_file: docker-compose.env
    volumes:
      - logs_reverse_proxy:/var/log/nginx
    logging:
      driver: json-file
      options:
        max-file: "10"
        max-size: "10m"
    ports:
      - "80:80"
      - "443:443"
    depends_on:
      - kheops-authorization
      - kheops-dicomweb-proxy
      - kheops-ui
      - kheops-zipper
    networks:
      - kheops_network
      - frontend_network
    extra_hosts:
      - "kheops01.psi.ch:127.0.0.1"
    secrets:
      - privkey.pem
      - fullchain.pem

  pacs-authorization-proxy:
    container_name: pacsauthorizationproxy
    env_file: docker-compose.env
    image: osirixfoundation/pacs-authorization-proxy:v1.0.8
    volumes:
      - logs_pep:/var/log/nginx
    logging:
      driver: json-file
      options:
        max-file: "10"
        max-size: "10m"
    depends_on:
      - pacs-arc
    secrets:
      - kheops_auth_hmasecret
      - kheops_auth_hmasecret_post
    networks:
      - pacs_network
      - kheops_network

#####################################################################
####   ELK                                                        ###
#####################################################################

  kibana:
    image: osirixfoundation/kheops-kibana:v17.0.0
    container_name: kibana
    networks:
      - elk_network
    ports:
      - 8081:5601

  logstash:
    image: osirixfoundation/kheops-logstash:v17.0.0
    container_name: logstash
    environment:
      - LS_JAVA_OPTS=-Xmx3g        
    networks:
      - elk_network
      - beats_network

  elasticsearch:
    image: osirixfoundation/kheops-elasticsearch:v17.0.0
    container_name: elasticsearch
    environment:
      - discovery.type=single-node
    volumes:
      - elastic_data:/usr/share/elasticsearch/data
    networks:
      - elk_network
    deploy:
      resources:
        limits:
          memory: 2gb

  kibana-initialize:
    image: osirixfoundation/kibana-initialize:main
    container_name: kibanainitialize
    networks:
      - elk_network

#####################################################################
####   Secret, volumes & Networks                                 ###
#####################################################################

secrets:
  kheops_authdb_pass:
    file: secrets/kheops_authdb_pass
  kheops_pacsdb_pass:
    file: secrets/kheops_pacsdb_pass
  kheops_auth_hmasecret:
    file: secrets/kheops_auth_hmasecret
  kheops_auth_hmasecret_post:
    file: secrets/kheops_auth_hmasecret_post
  kheops_client_dicomwebproxysecret:
    file: secrets/kheops_client_dicomwebproxysecret
  kheops_client_zippersecret:
    file: secrets/kheops_client_zippersecret
  privkey.pem:
    file: secrets/privkey.pem
  fullchain.pem:
    file: secrets/fullchain.pem


volumes:
  dcm4chee-db-data:
  dcm4chee-arc-wildfly:
  dcm4chee-arc-storage:
  kheops-db-data:
  elastic_data:
  logs_pep:
  logs_reverse_proxy:
  logs_auth:
  filebeat_registry:

networks:
# Network used for communication between the backend KHEOPS components
  kheops_network:
    driver: bridge
# The PACS network isolates the PACS from the reset of KHEOPS ensuring that
# all communication between KHEOPS and the PACS goes the the authorization
# proxy.
  pacs_network:
    driver: bridge
# Network reserved for communication with the frontend UI.
  frontend_network:
    driver: bridge
# Network for elastic stack (logstash-elasticsearch-kibana)
  elk_network:
    driver: bridge
# Network for beats (filebeat and metricbeat)
  beats_network:
    driver: bridge
