#!/bin/bash

#Script to get the expiration date of the Nvidia licenses for vGPU use
#We only check the first line of the result

export FLEXNETLS_BASEURL=http://cptvgpu03.psi.ch:7070/api/1.0/instances/~

cd /opt/flexnetls/nvidia/enterprise
./nvidialsadmin.sh -features|grep -m 1 Quadro|awk '{ print $5 }'|date -f - +"%s"
